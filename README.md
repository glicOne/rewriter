# Rewriter

```
$ git clone git://github.com/RomaniukVadim/rewriter.git
```
### text-rewriter

Using text translators, I made this program to rewrite blocks of text and spin the text using a thesaurus afterward to produce high quality text rewrites. Additionally, the rating system allows for finding the best processing language combinations. http://rewriter.smodin.me

![1](https://i.imgur.com/jksLUMV.png)

Rewriters (spinners) are expensive, work "alright" and are complicated. Rewriters have only existed for a short while, while translators have mountains of data, grasp of human language, and a longer history. Using translators with some other apps, we are able to successfully build a rewriter with a wide array of differentiation depending on processing languages.

### How It Works

English Text (start lang) => Processing Languages (start lang => spanish => polish => etc.) => Rewritten English Text (end lang)

### Addons
- Autocompletion
- Thesaurus
- Translator Choice (default: google)
- (future) Machine learning thesaurus library/suggestions

## App Build
- Languages (javascript)
- Frontend (React, graphql)
- Backend (node, express, mysql, graphql)
- Testing (mocha, enzyme, chai)

### Features to be added
- Finish rating system to find best processing language combo
- API
- Machine learning thesaurus
- Machine learning suggestions

### Code Quality
This app was created in a short time frame, features are being added, tests need to be fully developed, they have been tested on another version of the app but the code has since changed. The code will be better revised soon to be cleaner.


## ssfst (Subsequential Transducer for Efficient "Dictionary-Based" Text Rewriting)

Given an input text, produces a new text by applying a fixed set of rewrite rules. The algorithm uses the "leftmost largest match" replacement strategy with skips. No overlap between the replaced parts is possible. The time needed to compute the transducer is linear in the size of the input dictionary. For any  text `t` of length `|t|` the time it takes to perform a rewrite is `O(|t|+|t'|)` where `t'` denotes the resulting output string.  
Check out the [Online Sandbox](https://npm.runkit.com/ssfst).

### Example Usage
```js
const SSFST = require('ssfst');

const spellingCorrector = new SSFST([
    { input: 'acheive', output: 'achieve'},
    { input: 'arguement', output: 'argument'},
    { input: 'independant', output: 'independent'},
    { input: 'posession', output: 'possession'},
    { input: 'mercy less', output: 'merciless' }
]);

spellingCorrector.process('independant'); // => "independent"
spellingCorrector.process('mercy less arguement'); // => "merciless argument"
spellingCorrector.process('they acheived a lot'); // => "they achieved a lot"
```
```js
const transducer = new SSFST([
    { input: ' dog ', output: '<a href="https://en.wikipedia.org/wiki/Dog">dog</a>' },
    { input: ' fox ', output: '<a href="https://en.wikipedia.org/wiki/Fox">fox</a>' }
]);

transducer.process('The quick brown fox jumped over the lazy dog.');
/* => The quick brown <a href="https://en.wikipedia.org/wiki/Fox">fox</a> jumped over the lazy <a href="https://en.wikipedia.org/wiki/Dog">dog</a>. */
```


### Requirements
* [git](https://git-scm.com/downloads)
* [nodejs](https://nodejs.org/en/download/current/) v. >= 8.x

### Install via NPM
```
npm i --save ssfst
```

### Clone
```
git clone https://github.com/deniskyashif/ssfst.git ssfst
cd ssfst
npm i
```

### Run the Example
```
npm start
```

### Run the Tests
```
npm test
```

### Code Coverage
```
npm run cover
```

### References
* ["Efficient Dictionary-Based Text Rewriting using Subsequential Transducers" by S. Mihov, K. Schulz](https://www.researchgate.net/publication/232005152_Efficient_dictionary-based_text_rewriting_using_subsequential_transducers)
* ["Finitely Subsequential Transducers" by C. Allauzen, M. Mohri](https://www.researchgate.net/publication/263878442_FINITELY_SUBSEQUENTIAL_TRANSDUCERS)

## tnpy

Tn is a language developed by Desert and tan for matching, translating, and extracting text (DSL). And for its development and optimization of a dedicated compiler. Based on recursive descent methods and regular expressions, natural text can be parsed and converted into trees and dictionaries, identifying complex sequence patterns such as time, address, and quantity.
Github Address: https://github.com/ferventdesert/tnpy

Grammar introduction

## 0. Reasons for design

 String parsing and processing is almost a must-have task for each member program, simple to segment strings like "1,2,3,4", a bit more complex such as string matching, and then more complex such as compiling and parsing SQL syntax . Strings have almost unlimited expressive power. Solving the string problem solves 90% of the computer's problems.

  Although string processing is deeply rooted in people's minds, when dividing a character, it is originally divided by a comma. Suddenly, a semicolon appears and the program may go wrong. Again, like the date processing, each programmer must have a headache for all kinds of strange time expressions, and it is very time-consuming to process. These functions can only be hard coded. They are the lowest level modules that interact with the outside world, but they are so fragile.

>* How to convert "123" to digital?
>* How to recognize "December 14, 2013" as time and convert it to time
Inter-type?
>* How to parse an XML or JSON file?

Although regular expressions provide a powerful matching function and become a necessary tool, it has many limitations. We have extended the regular expression engine to greatly enhance its capabilities.
Online demo: http://www.desertlambda.com:81/extracttext.html

## 1. How to learn?

Basically, programmers have read the article "30 minutes to learn regular expressions"? In the end, few people can read it in 30 minutes. But believe me, the TN engine only takes 15 minutes to learn.

Detailed syntax instructions are here:

[tn basic syntax][1]

[Use tn to construct a natural language calculator][2]

[tn implementation of xml parser][3]

TN can implement text matching, transcription and information extraction, which can be understood as the inverse operation of the template engine. Simple operations are more convenient with regular expressions, but many problems are beyond the scope of regularity. Then you need to use TN.

The TN interpreter is available in Python, C# and C versions. The C# version is no longer maintained. In languages ​​such as C# or Java, cross-language compilation using IronPython or Jython is recommended.

Tnpy is tn's Python interpreter. Python's good readability makes writing code very convenient. The code does not exceed 1000 lines, single file, no third-party library dependencies. It is recommended to use Python3.

Tn is an interpreted language. It needs to write a rules file and load it with tnpy and then process the text.

## 1. Basic matching and replacement:

First of all, we first write a simple rule file learn, as follows:

```
#%Order% 1
Hello= ("Hello");
```
Next, execute the following python code:
```
From src.tnpy import RegexCore
Core = RegexCore('../rules/learn')
Matchs=core.Match('Leadership for leadership, hello to your wife');
For m in matchs:
    Print('match',m.mstr, 'pos:',m.pos)
```
Introduced the tnpy namespace, after which the engine is initialized from the learn rules file and matches the text:

```
Success load tn rules:../rules/learn
Match Hello pos: 2
Match Hello pos: 7
```

Above the output of the text of the matching results and location. Of course, this can also be done.

If we match the `leadership hello, my wife Hello, and would like to transfer all `Hello` and `Hello` to `hello`.

To do this we add two child rules of hello2 and hello3:

```
Hello2= $(hello)| ("Hello");
#%Order% 1
Hello3= $(hello2) : (//:/hello/);
```

`hello2` refers to the ``hello`` rule, and ``hello'' is also added.

Hello3 is the main rule that is responsible for translating the content of `hello2` into `hello`

`($ means to quote a rule, | means to put several rules side by side, matching the longest rule, : means to write.)

Execute the following code:

```
Print(core.Rewrite('Leadership! Hello wife'));
```

The result is:
```
Leadership hello! Wife hello
```

If we want to replace the order, put "hello" in front of it? It can be written like this:

```
People= ("wife") | ("Leadership");
#%Order% 1
Reorder= $(people) $(hello3) : $2 $1;
```

First use `people` to define how to describe `wife, lead`, and then use reorder to modify the order, note that reorder is a sequential structure**, people match the wife and leader, hello3 matches hello/hello, and converts it to `hello`. `$2 and $1` modify the transfer order, output after executing Rewrite:

```
Hello leadership! Hello wife
```

We call a structure like `$(name1)$(name2)` as a sequential expression and `$(name1) | $(name2)` as an expression or expression.
If you have just drawn all the rules into a map, it looks like this:

![foo.png-34.5kB][4]

## 2. Regular Expressions

Using only text, performance is poor. We introduce regular expressions to complete, regular expressions need to be placed in (//), pay attention to the difference between text ("").

If you are transliteration, it is marked as `(/match/:/rewrite/)`; The following expression converts all long blanks to one whitespace:

```
Byte_det_space = (/ */://);
```

Convert all letters to blanks below:

```
Low_letter_to_null = (/[a-z]/ );
# or below:
Low_letter= (/[a-z]/);
Translate= $(low_letter) : ("");
```

Think there is no challenge? We then look at the following.

### 3. Complex Composition: Chinese Numbers to Arabic Numbers

Twenty-three how to convert to 23? This is more difficult with ordinary programming. When we try to solve it with TN, we will find that it is not difficult at all.
First define the conversion of Chinese characters from one to two, three to nine to 1-9, and you will certainly write this rule:

```
# Definition 0-9
Int_1 = ("a" : "1");
Int_0 = ("Zero" : "0");
Int_2 = ("two" : "2") | ("two" : "2");
Int_3_9 = ("Three" : "3") | ("Four" : "4") | ("Five" : "5") | ("Six" : "6") | ("Seven" : "7" ) | ("Eight": "8") | ("Nine": "9");
Int_1_9 = $(int_1) | $(int_2) | $(int_3_9) | (/\d/);
Int_0_9 = $(int_0) | $(int_1_9);
Int_del_0 = (/zero/ : /0/) | (// : /0/);
Int_0_9_null = $(int_del_0) | $(int_0_9);
```

The reason why 0, 1, and 2 should be written separately is because these numbers have special circumstances. For example, both two and two represent two and need to be dealt with later.
With the `int_0_9_null` rule above, you can write `5702` to `5702`. However, there is no way to handle the situation of `Twenty-Three`.

Then define the following rules so that `13` can be written as `13`

```
Int_del_0 = (/zero/ : /0/) | (// : /0/);
Int_0_9_null = $(int_del_0) | $(int_0_9);
#define 10, ten
Int_1_decades = (/ten/ : /1/) | (/a ten/ : /1/);
```

In addition to the following rules, int_1_9_decades defines how ten digits are transposed, while int_10_99 defines transliteration rules from ten to ninety-nine.

```
Int_10_99 = $(int_1_9_decades) $(int_0_9_null) | (/[1-9][0-9]/) ;
Int_1_99 = $(int_1_9) | $(int_10_99) ;
Int_01_99 = $(int_1_9) | $(int_10_99) | (/\d{1,2}/);

#%Order% 3
Int_0_99 = $(int_0) | $(int_1_9) | $(int_10_99);
```

Take a look at the following example:
`print({r:core.Rewrite(r) for r in ['10', '37', '13', '68']});
operation result:
`{'Thirteen': '13', '68': '68', 'Ten': '10', 'Thirty-seven': '37'}`
Is it amazing? How was 37 transferred to 37?

Look carefully at the rules. The rules are built from the bottom up into a rule tree. In_0_99 is the root node of the entire tree. The structure is as follows:
![foo.png-132.1kB][5]
The following log file gives the matching process:

```
int_0_99,Table,Raw  =thirty-seven
  int_0,String,Raw  =thirty-seven
  int_0,String,NG
  int_1_9,Table,Raw  =thirty-seven
    int_1,String,Raw  =thirty-seven
    int_1,String,NG
    int_2,Table,Raw  =thirty-seven
      int_2_merge,Regex,Raw  =thirty-seven
      int_2_merge,Regex,NG
    int_2,Table,NG
    int_3_9,Table,Raw  =thirty-seven
      int_3_9_merge,Regex,Raw  =thirty-seven
      int_3_9_merge,Regex,Match=three
    int_3_9,Table,Match=three
    int_1_9_3,Regex,Raw  =thirty-seven
    int_1_9_3,Regex,NG
  int_1_9,Table,Match=three
  int_10_99,Table,Raw  =thirty-seven
    int_10_99_0,Sequence,Raw  =thirty-seven
      int_1_9_decades,Table,Raw  =thirty-seven
        int_1_decades,Table,Raw  =thirty-seven
          int_1_decades_0,Regex,Raw  =thirty-seven
          int_1_decades_0,Regex,Match=ten
          int_1_decades_1,Regex,Raw  =thirty-seven
          int_1_decades_1,Regex,NG
        int_1_decades,Table,Match=ten
        int_1_9_decades_1,Sequence,Raw  =thirty-seven
          int_1_9,Table,Raw  =thirty-seven
          int_1_9,Table,Buff =three
          unknown,Regex,Raw  =Seventeen
          unknown,Regex,Match=ten
        int_1_9_decades_1,Sequence,Match=thirty
      int_1_9_decades,Table,Match=thirty
      int_0_9_null,Table,Raw  =Seven
        int_del_0,Table,Raw  =Seven
          int_del_0_0,Regex,Raw  =Seven
          int_del_0_0,Regex,NG
          int_del_0_1,Regex,Raw  =Seven
          int_del_0_1,Regex,Match=
        int_del_0,Table,Match=
        int_0_9,Table,Raw  =Seven
          int_0,String,Raw  =Seven
          int_0,String,NG
          int_1_9,Table,Raw  =Seven
            int_1,String,Raw  =Seven
            int_1,String,NG
            int_2,Table,Raw  =Seven
              int_2_merge,Regex,Raw  =Seven
              int_2_merge,Regex,NG
            int_2,Table,NG
            int_3_9,Table,Raw  =Seven
              int_3_9_merge,Regex,Raw  =Seven
              int_3_9_merge,Regex,Match=Seven
            int_3_9,Table,Match=Seven
            int_1_9_3,Regex,Raw  =Seven
            int_1_9_3,Regex,NG
          int_1_9,Table,Match=Seven
        int_0_9,Table,Match=Seven
      int_0_9_null,Table,Match=Seven
    int_10_99_0,Sequence,Match=thirty-seven
    int_10_99_1,Regex,Raw  =thirty-seven
    int_10_99_1,Regex,NG
  int_10_99,Table,Match=thirty-seven
int_0_99,Table,Match=thirty-seven
```
The engine looks for the longest text along the rule tree from left to right of the text. If any step on a sequential expression fails, the entire sequence expression is discarded. The or expression will iterate through each sub-expression until the longest one is found, and the result is returned. Specific matching principles, as well as optimization, will be introduced in a dedicated article.

### 4. Construct more complex rules from rules

Naturally, know how to define thirty-seven, you can define 537, which is just `int_1_9_hundreds+int_0_99` (this is already defined).

```
Int_1_9_hundreds = $(int_1_9) ("hundred" : "");
Int_100_999 = $(int_1_9_hundreds) ("" : "00") | $(int_1_9_hundreds) $(int_10_99);
Int_1_999 = $(int_1_99) | $(int_100_999);
```

ʻint_1_999` can handle such issues as 537!

Furthermore, we can deal with thousands or tens of thousands. After extending it to tens of thousands, we can spontaneously derive millions, trillions of expressions.

How to deal with negative numbers? This is not simple!

```
Signed_symbol0 = ("positive" : "") | ("negative" : "-") | ("positive and negative" : "±") | ("\+" : "+") | ("\-" : " -") | ("±" : "±") ;
Signed_symbol = $(signed_symbol0) | $(null_2_null);
```

Next, we default to a positive integer as `integer_int`, then the integer (including positive and negative) is:

`integer_signed = $(signed_symbol) $(integer_int)`

### 5. Property Extraction
Along the way just now, we naturally can define the score, but it is not enough to transfer it. When we meet one-third, we not only need to treat it as 1/3, but also calculate its value. This involves To attribute extraction. That is, the information is extracted from the text as a dictionary.

The score, however, is `integer+part+integer`, which can be defined in the following form:

```
Fraction_cnv_slash = ("Partition" : "/");
Fraction2 = ("/" : "/");
Percent_transform= ("%" : "100") | ("‰" : "1000");
#%Type% DOUBLE
#%Property% Denominator,,Numerator| Numerator ,, Denominator | Denominator ,, Numerator
#%Order% 101
Fraction = $(integer_int_extend) $(fraction_cnv_slash) $(integer_int) : $3 $2 $1
    $(integer_int) $(fraction2) $(integer_int)
    $(pure_decimal) ("" : "/") $(percent_transform);
```

This is a bit complicated, but let me explain slowly. There are three kinds of scores, such as just ``one third``, or ``1/3``, or ``30%``. Corresponds to the three sub-rules of the above `fraction` rule. It is not difficult to understand the rules carefully.

It is worth noting that the Property tag, which defines how to extract information. Also separated by a vertical bar, each name corresponds to one of the following sub-rules, which is an empty skip. In "Thirteen of Thirteenths," "Thirteen" corresponds to Numerator, and "Twenty-Four" corresponds to Denominator. Let's test:

`print(core.Extract('Twenty-Fourteen', entities=[core.Entities['fraction']]))`

We use the Extract function to extract the text. A dictionary is returned. entites are optional parameters. We limit the use of only the fractional rules to match and get the output:

```
[{'Numerator': '24', '#rewrite': '24/13', '#type': 'fraction',
'#match': 'Twenty-fourths', 'Denominator': '13', '#pos': 3}]
```

Is it great?

#### 6. Embedding Python Scripts

There is a need that hasn't been talked about. Converting all uppercase letters to lowercase letters, you may want to define 26 string rules and concatenate them with expressions. This is too much trouble. We can directly do this:

`low_to_up_letter = (/[A-Z]/) : "str.lower(mt)";`

`[AZ]` matches all capital letters and sends the matching result to the second half of the transfer. The built-in interpreter will execute the python code and convert it to lower case, mt represents the match string of the previous expression. Rt stands for transfer string. Fortunately, `[A-Z]` doesn't perform transfer, you can think of `mt==rt`.

This is an example of embedding python during the transfer process. It also embeds transposes when matching:

`foo = "findsecret" : "print(mt)"`;

The previous findsecret function is responsible for finding the "mystery text" in the string, the subsequent transfer code is printed, and the original character is returned...

### 6. Did you finish reading in 15 minutes?

I believe you don't, because it takes at least five minutes to read the log file that matches the rules, but if you have a compiler theory and a regular basis, you can quickly understand it. It took us a year to develop this engine from scratch to optimize and perfect it repeatedly.

After defining the various numbers, we can quickly define the time, date, phone number, address... and what you see is just the tip of the TN language.

 - It can analyze the pattern of the text and parse a sequence such as ABCABC to find that it is a repeating pattern.

 - Not only can they be matched in sequence but also reversed or even out-of-order. This can lead to questions like “school mottos”.

 - Rules can call themselves, in conjunction with scripts, thus enabling recursive descent parsing. For example, 30 lines of code implement xml parsing, or 20 lines of rules implement natural language calculators.

 - Rules can be embedded in scripts or even dynamically generated code, so even in theory, TN can compile itself.

 - TN can also be a simple SQL interpreter, or a tool for simple cross-translation between Chinese and English.

Is it trembling with excitement? The only thing that limits your ability is your imagination. This blog will further publish a series of content about tn, including advanced grammar,
Tn optimization and so on.

Interested can contact the author


  [1]: http://www.cnblogs.com/buptzym/p/5355827.html
  [2]: http://www.cnblogs.com/buptzym/p/5361121.html
  [3]: http://www.cnblogs.com/buptzym/p/5355920.html
  [4]: http://static.zybuluo.com/buptzym/ksl5ggrfcn1psmdf2f81i8wg/foo.png
  [5]: http://static.zybuluo.com/buptzym/itwhlmz8ua2h3jgbqdq5z48g/foo.png
